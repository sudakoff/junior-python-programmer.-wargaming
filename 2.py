import collections

a_list = [34, 12, 412, 4, 75, 0, 54, 5454]


class TestClass1(object):
    """Класс 1"""

    def __init__(self, a_list, value):
        """Constructor"""
        self.a_list = a_list
        self.value = value

    def calculation(self):
        a_list.append(value)
        a_list.pop(0)
        return a_list


value = int(input('Введите число для теста Класса 1: '))
print(TestClass1.calculation(value))


class TestClass2(object):
    """Класс 2"""

    def __init__(self, a_list, value2):
        """Constructor"""
        self.a_list = a_list
        self.value = value2

    def calculation_deque(self):
        foo = collections.deque(a_list, maxlen=7)
        foo.append(value2)
        return foo


value2 = int(input('Введите число для теста Класса 2: '))
print(TestClass2.calculation_deque(value2))

# -----------------------------------------------------------
# Первый класс просто принимает массив и удаляет из него элемент по принципу "Первый пришел - первый ушел".
# Второй класс более функционален, можно ограничить длину массива, но требует импорт класса deque из модуля collections.
# Как понимаю, заместо deque можно использовать и numpy.roll, он быстрее.
# Конечно, можно создать и класс с телом a_list = a_list[1:] + [value]
#
# Судаков Артём
# email sviside@ya.ru
# -----------------------------------------------------------
