def partition(nums, low, high):
    pivot = nums[(low + high) // 2]
    i = low - 1
    j = high + 1
    while True:
        i += 1
        while nums[i] < pivot:
            i += 1
        j -= 1
        while nums[j] > pivot:
            j -= 1
        if i >= j:
            return j

        nums[i], nums[j] = nums[j], nums[i]


def quick_sort(nums):
    def _quick_sort(items, low, high):
        if low < high:
            split_index = partition(items, low, high)
            _quick_sort(items, low, split_index)
            _quick_sort(items, split_index + 1, high)

    _quick_sort(nums, 0, len(nums) - 1)


a_list = [34, 12, 412, 4, 75, 0, 54, 5454]
quick_sort(a_list)
print(a_list)

# -----------------------------------------------------------
# Сортировка Хоара считается наиболее быстрой. В процессе решения
# ознакомился с множеством решений и тестов (благо на Хабре и в сети их множество).
# Как понимаю, это наиболее оптимальный вариант, если
# опорный элемент НЕ равен наименьшему или наибольшему элементам списка.
#
# Судаков Артём
# email sviside@ya.ru
# -----------------------------------------------------------
